import React, { Component } from 'react';
import Card from '../Card/Card';
import "./Content.css"

class Content extends Component {
    constructor() {
        super();
        this.state = {
            firstName: "Adam",
            alamat: "California"
        }
    }


    render() {
        return (
            <div className="in">
                <Card firstName={this.state.firstName} alamat={this.state.alamat}/>
                <button onClick={() => this.setState({firstName: "Adam Surya Wibawa"})}>Nama Asli</button>
                <button onClick={() => this.setState({alamat: "Asli Rusia"})}>Alamat Asli</button>
            </div>
        )
    }
}

export default Content;