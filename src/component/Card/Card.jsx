import React, { Component } from 'react';
import "./Card.css"

class Card extends Component {
    constructor(props) {
        super(props);

    }

    render() {
        return(
            <div className="paragraf">
                <p>Halo Nama Saya {this.props.firstName}</p>
                <p>Asal saya {this.props.alamat}</p>
            </div>
        )
    }
}

export default Card;